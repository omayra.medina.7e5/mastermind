package cat.itb.omayramedina7e5.dam.m03.uf1.iterative.project

import java.util.*


data class Evaluation(val rightPosition: Int, val wrongPosition: Int)


fun main() {
    // Use only this scanner
    val scanner = Scanner(System.`in`)
    // Your code
    val start = 1
    while (start == 1) {
        println("Vols jugar en mode 1vs1 (1) o solitari (2)?")
        var mode = scanner.nextInt()
        if (mode == 1) {
            println("Introdueix la paraula secreta")
            val secretWord = scanner.nextLine()
        }
        if (mode == 2) {
            println("Comença el joc! \nIntrodueix una combinació")
            val combination = scanner.nextLine()
        }
        break
    }

    //use this function
    var evaluation = evaluateWord("ABCD", "AEEE")

fun randomChar():Char{
    val allowedChars = 'A'..'F'
    return allowedChars.random()
}

fun evaluateWord(secret: String, guess: String): Evaluation {
    //Calculate your right and wrong positions and change it as you need
    return Evaluation(0,0)
}
}